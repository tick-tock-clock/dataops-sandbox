#!/usr/bin/env bash

set -ex

awslocal s3 mb s3://streaming
awslocal s3 mb s3://bronze-tables
awslocal s3 mb s3://silver-tables
awslocal s3 mb s3://gold-tables