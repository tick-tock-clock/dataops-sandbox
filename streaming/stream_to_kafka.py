import itertools
from datetime import datetime, timedelta
from random import randint, random, randrange
from sys import argv, exit
from time import sleep
from uuid import uuid4

from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer

VALUE_SCHEMA_PATH = 'schemas/orders-value.json'
KEY_SCHEMA_PATH = 'schemas/orders-key.json'


class DataGenerator:
    def __init__(self):
        super(DataGenerator, self).__init__()
        self.id_generator = itertools.count()

    def data(self):
        now = datetime.now()
        return {
            'event_id': str(next(self.id_generator)),
            'event_timestamp': int(now.timestamp()),
            'event_timestamp_string': now.isoformat(),
            'uuid': uuid4().hex,
            'basket_price': round(random() * randrange(100, 1000), 2)
        }


def delivery_report(err, msg):
    """ Called once for each message produced to indicate delivery result.
        Triggered by poll() or flush(). """
    if err is not None:
        print('Message delivery failed: {}'.format(err))
    else:
        print(f'Message delivered to {msg.topic()}, partition[{msg.partition()}], timestamp: {msg.timestamp()}')


def read_schema(path: str) -> str:
    with open(path) as json_file:
        return json_file.read()


if __name__ == '__main__':
    if len(argv) < 3:
        print('''
        Program arguments: <bootstrap.servers (comma separated)> <topic> <max_run_time (seconds)>
        ''')
        exit(-1)
    bootstrap_servers: str = argv[1]
    topic: str = argv[2]
    max_run_time: int = argv[3] if argv[3:] else 6000

    producer = AvroProducer({
        'bootstrap.servers': bootstrap_servers,
        'on_delivery': delivery_report,
        'schema.registry.url': 'http://0.0.0.0:8081'
    }, default_key_schema=avro.loads(read_schema(KEY_SCHEMA_PATH)),
        default_value_schema=avro.loads(read_schema(VALUE_SCHEMA_PATH)))

    data_generator = DataGenerator()

    end_time = datetime.now() + timedelta(seconds=max_run_time)

    current_time = datetime.now()

    try:
        while current_time < end_time:
            producer.produce(topic=topic, key={'key': '1'}, value=data_generator.data())
            producer.flush()
            sleep(randint(1, 5))
            current_time = datetime.now()
    except KeyboardInterrupt:
        print('Program terminated!')
